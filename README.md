# Very simple test for SQLite database migrations
The purpose of this project is to provide a very simple script for testing database migrations in a project.

# Requirements
- [Go](https://golang.org/) language environment installed
  - see [Getting started](https://golang.org/doc/install)

# Instructions
- Install the project with:
  - `go get bitbucket.org/jsantiagoh/db-migrations`
- `cd $GOPATH/src/bitbucket.org/jsantiagoh/db-migrations`
- `go run migrations.go --basedir $BASE_DIR_FOR_MIGRATIONS`

Note that the directory `$BASE_DIR_FOR_MIGRATIONS` must contain a structure similar to the `sql` directory in the base project with each version as a subdirectory containing the following files:
- schema.sql (Mandatory): contains the schema to install in a new installation from scratch
- on_update.sql (Optional): Indicates how to migrate from one version to the next
