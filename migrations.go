package main

import (
	"database/sql"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	_ "github.com/mattn/go-sqlite3"
)

var baseDir = flag.String("basedir", "sql", "base directory for versions")

func main() {
	flag.Parse()

	versions := getVersions(baseDir)
	log.Println("all versions:", versions)
	permutations := permuteAsc(versions)
	log.Println("all permutations: ", permutations)
	for _, v := range permutations {
		log.Printf("* testing migration %v -> %v", v[0], v[1])
		err := testMigration(*baseDir, v[0], v[1])
		if err != nil {
			log.Fatalf("ERROR migrating %v -> %v : %v", v[0], v[1], err)
			panic(err)
		}
		log.Printf(": OK\n")
	}
}

func testMigration(base, from, to string) error {
	os.Remove("./database.db")
	db, err := sql.Open("sqlite3", "./database.db")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// Read origin schema
	schema, err := readSQL(base, from, "schema.sql")
	if err != nil {
		return fmt.Errorf("error reading schema.sql: %v", err)
	}

	// Install origin schema
	err = run(db, schema)
	if err != nil {
		return err
	}

	// Run Update from the to version
	schema, err = readSQL(base, to, "on_update.sql")
	if err != nil {
		log.Printf("ignoring %v because: %v", schema, err)
	}

	return nil
}

func run(db *sql.DB, statement string) error {
	_, err := db.Exec(statement)
	return err
}

func readSQL(base, from, file string) (string, error) {
	bytes, err := ioutil.ReadFile(filepath.Join(base, from, file))
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func getVersions(dir *string) []string {
	dirs, err := ioutil.ReadDir(*baseDir)
	if err != nil {
		panic(err)
	}
	versions := make([]string, len(dirs))
	for i, dir := range dirs {
		if dir.IsDir() && dir.Name() != "" {
			versions[i] = dir.Name()
		} else {
			log.Println("ignoring non directory", dir)
		}
	}
	return versions
}

// Calculates the sum of the numbers until n, see divergent series
func sum(n int) int {
	return (n * (n + 1)) / 2
}

func permuteAsc(versions []string) [][]string {
	n := sum(len(versions) - 1)
	perm := make([][]string, n)
	k := 0
	for i := 0; i < len(versions); i++ {
		for j := i + 1; j < len(versions); j++ {
			perm[k] = make([]string, 2)
			perm[k][0] = versions[i]
			perm[k][1] = versions[j]
			k = k + 1
		}
	}
	return perm
}
